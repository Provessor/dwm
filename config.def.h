/* See LICENSE file for copyright and license details. */

/* appearance */
static const int smartborder        = 1;        /* disable border when only one window */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 1;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "JetBrainsMono Nerd Font Mono:size=10" };
static const char unsel_bg[]        = "#ECF0F1";
static const char unsel_fg[]        = "#486684";
static const char sel_bg[]          = "#B6C5C8";
static const char sel_fg[]          = "#486684";
static const char *colors[][3]      = {
	/*               fg		bg		border   */
	[SchemeNorm] = { unsel_fg,	unsel_bg,	unsel_bg },
	[SchemeSel]  = { sel_fg,	sel_bg,		sel_bg   },
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"keepassxc", NULL };
const char *spcmd2[] = {"x-terminal-emulator", "--class", "dropdown", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{ "keepassxc",   spcmd1 },
	{ "dropdown",    spcmd2 },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                 	instance	title	tags mask	iscentered	isfloating	monitor */
	{ "Gimp",                	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Firefox-esr",         	"Places",   	NULL,	0,       	1,		1,		-1 },
	{ "mpv",                 	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-adapters",    	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-applet",      	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-assistant",   	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-browse",      	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-manager",     	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-report",      	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-sendto",      	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Blueman-services",    	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ "Nm-connection-editor",	NULL,       	NULL,	0,       	1,		1,		-1 },
	{ NULL,                  	"keepassxc",	NULL,	SPTAG(0),	1,		1,		-1 },
	{ NULL,                  	"dropdown", 	NULL,	SPTAG(1),	1,		1,		-1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "x-terminal-emulator", NULL };

static const char *editor[]      = { "emacsclient", "-c", NULL };
static const char *browser[]     = { "firefox", NULL };
static const char *filemanager[] = { "x-terminal-emulator" , "-t", "lf", "-e", "startlf", NULL };
static const char *mounter[]     = { "dmenumount", NULL };
static const char *umounter[]    = { "dmenuumount", NULL };

static const char *locker[]    = { "loginctl", "lock-session", NULL };
static const char *scroff[]    = { "xset", "dpms", "force", "suspend", NULL };
static const char *poweroff[]  = { "systemctl", "poweroff", NULL };
static const char *reboot[]    = { "systemctl", "reboot", NULL };
static const char *hibernate[] = { "systemctl", "hibernate", NULL };

static const char *volitog[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "toggle", NULL };
static const char *volidec[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "5%-", NULL };
static const char *voliinc[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "5%+", NULL };
static const char *volltog[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "0%", NULL };
static const char *volldec[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "1%-", NULL };
static const char *vollinc[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "1%+", NULL };
static const char *volbtog[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "100%", NULL };
static const char *volbdec[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "10%-", NULL };
static const char *volbinc[] = { "amixer", "-q", "-D", "pulse", "sset", "Master", "10%+", NULL };

static const char *briidec[] = { "xbacklight", "-dec", "5", NULL };
static const char *briiinc[] = { "xbacklight", "-inc", "5", NULL };
static const char *brildec[] = { "xbacklight", "-dec", "1", NULL };
static const char *brilinc[] = { "xbacklight", "-inc", "1", NULL };
static const char *bribdec[] = { "xbacklight", "-dec", "10", NULL };
static const char *bribinc[] = { "xbacklight", "-inc", "10", NULL };

#include <X11/XF86keysym.h>
#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,            	XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,  	XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,  	XK_f,      spawn,          {.v = filemanager } },
	{ MODKEY,            	XK_o,      spawn,          {.v = editor } },
	{ MODKEY,            	XK_b,      spawn,          {.v = browser } },
	{ MODKEY|ShiftMask,  	XK_b,      togglebar,      {0} },
	{ MODKEY,            	XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,            	XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,            	XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,            	XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,            	XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,            	XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,  	XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,  	XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,            	XK_g,      centerwindow,   {0} },
	{ MODKEY,            	XK_Return, zoom,           {0} },
	{ MODKEY,            	XK_Tab,    view,           {0} },
	{ MODKEY,            	XK_x,      killclient,     {0} },
	{ MODKEY,            	XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,            	XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,            	XK_e,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,            	XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,  	XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,  	XK_e,      togglefullscr,  {0} },
	{ MODKEY,            	XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,  	XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,            	XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,            	XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,  	XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,  	XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,  	XK_p,  	   togglescratch,  {.ui = 0 } },
	{ MODKEY,            	XK_u,	   togglescratch,  {.ui = 1 } },
	{ MODKEY,            	XK_v,      spawn,          {.v = mounter } },
	{ MODKEY|ShiftMask,  	XK_v,      spawn,          {.v = umounter } },
	{ MODKEY|ControlMask,	XK_q,      quit,           {0} },
	{ MODKEY|ControlMask,	XK_e,      spawn,          {.v = poweroff } },
	{ MODKEY|ControlMask,	XK_r,      spawn,          {.v = reboot } },
	{ MODKEY|ControlMask,	XK_t,      spawn,          {.v = hibernate } },
	TAGKEYS(             	XK_1,                      0)
	TAGKEYS(             	XK_2,                      1)
	TAGKEYS(             	XK_3,                      2)
	TAGKEYS(             	XK_4,                      3)
	TAGKEYS(             	XK_5,                      4)
	TAGKEYS(             	XK_6,                      5)
	TAGKEYS(             	XK_7,                      6)
	TAGKEYS(             	XK_8,                      7)
	TAGKEYS(             	XK_9,                      8)
	{ 0,               	XF86XK_ScreenSaver,       spawn, {.v = locker  } },
	{ 0,               	XF86XK_Battery,           spawn, {.v = scroff  } },
	{ 0,               	XF86XK_AudioMute,         spawn, {.v = volitog } },
	{ 0,               	XF86XK_AudioLowerVolume,  spawn, {.v = volidec } },
	{ 0,               	XF86XK_AudioRaiseVolume,  spawn, {.v = voliinc } },
	{ ControlMask,      	XF86XK_AudioMute,         spawn, {.v = volltog } },
	{ ControlMask,      	XF86XK_AudioLowerVolume,  spawn, {.v = volldec } },
	{ ControlMask,      	XF86XK_AudioRaiseVolume,  spawn, {.v = vollinc } },
	{ ShiftMask,        	XF86XK_AudioMute,         spawn, {.v = volbtog } },
	{ ShiftMask,        	XF86XK_AudioLowerVolume,  spawn, {.v = volbdec } },
	{ ShiftMask,        	XF86XK_AudioRaiseVolume,  spawn, {.v = volbinc } },
	{ 0,               	XF86XK_MonBrightnessDown, spawn, {.v = briidec } },
    { 0,               	XF86XK_MonBrightnessUp,   spawn, {.v = briiinc } },
	{ ControlMask,     	XF86XK_MonBrightnessDown, spawn, {.v = brildec } },
    { ControlMask,     	XF86XK_MonBrightnessUp,   spawn, {.v = brilinc } },
	{ ShiftMask,       	XF86XK_MonBrightnessDown, spawn, {.v = bribdec } },
    { ShiftMask,       	XF86XK_MonBrightnessUp,   spawn, {.v = bribinc } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

